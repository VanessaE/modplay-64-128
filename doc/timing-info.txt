REU DRIVER TIMING INFO

Assuming stock C64/128, 4-bit SID stereo, no accelerator/turbo, C64 screen
turned on unless otherwise noted.  All times rounded-up to the next whole
cycle.

-----------
fetch DMA calls alone:

On average, the step rate will be close to 1.7, so it'll be fetching
16 * 1.7 = ~27 bytes per transfer.  We have to split the transfer up into
smaller pieces to avoid too much impact on the audio ISR.  With "rounding",
the average will be closer to 32 bytes.  At any rate, this costs some
performance but what else can we do?

2 +4+4 +4+4 +4+4 +4+4 +32 = 66 cycles per track, total 264 cycles

Worst possible case would be playing the highest note, B-4, which would have
a step rate of about 8.688, so fetching 16 * 8.333 = ~139 bytes without any
kind of cap or limiter:

2 +4+4 +4+4 +4+4 +4+4 +139 = 173 cycles per track, total 692 cycles

For the C128, the hardware does clock stretching on I/O accesses, but only
for the one cycle when the actual read/write is being committed.  Most of
the fetch instructions, and all of the actual DMA fetches, run in 1 MHz mode
for their whole duration. The explicit 1/2 MHz switching above adds 18,
2 MHz cycles (9 for the shift down to 1 MHz, 9 to shift back up).

Being almost twice as fast, it'll have half the step rates, so best step rate
will be about 0.137, typical will be around 0.9, worst around 6.4, so it'll
be fetching ~2, ~13, and ~101 bytes, respectively.

That machine's fetch timings should be about:

most likely:    2 +4+8 +8+8 +8+8 +8+8 +13 + 18 = 93 cycles, total 372
worst possible: 2 +4+8 +8+8 +8+8 +8+8 +101 + 13 = 176 cycles, total 704

On SuperCPU each fetch will probably only need to get two bytes or so on
average, since the whole fetch-mix-output chain is already running several
times faster than stock:

(20+20+(2*20)) * 4 = 320 turbo cycles per track
multiply by 4 = 1280 turbo cycles for all four tracks

We'll just assume a worst-case of 3 times that amount, or 3840 turbo cycles.

----------
Fetch loop

open:
2+4+2+2 = 10 cycles

within the loop, not counting the above DMA calls, (those are added below):

minimum time (also the most likely case), all four tracks updating two
bytes of the 4-byte address, and not at the end of the sample yet:

(3+2+3 +2+3+4+3 +3+4 +3+4)*4 + (3+2+3+2+3+2+3)*4 = 208 cycles

worst possible case, all four tracks have to update all four bytes, not at
the end of sample yet:

(3+2+3+2 +2+3+4+2 +5+3+4+2 +5+3+4+3)*4 + (3+2+3+2+3+2+3)*4 = 272 cycles

fetch routine main loop close:
5+2+6 = 13 cycles

On SuperCPU-equipped machines, accounting for cache misses:

open loop:
(2+20+2+2) = 26 turbo cycles

within loop, most likely code path:
2+20+3+2+3+3+2+3+20+3 +3+20 +3+20 +3+2+3+2+3+2+3 +2 = 127 cycles per track
multiply by 4 = 508 turbo cycles for all four tracks

We'll assume double that amount for the worst case, so 1016 turbo cycles.

close loop:
5+2+3 = 10 turbo cycles

Total fetch runtime for all four tracks,
amortized across 16 output samples:

C64:
	likely: (10 + 208 + 264 + 13) / 16 = ~31 cycles
	worst:  (10 + 272 + 692 + 13) / 16 = ~62 cycles

C128:
	likely: (10 + 208 + 372 + 11) / 16 = ~38 cycles
	worst:  (10 + 272 + 704 + 11) / 16 = ~63 cycles

SCPU:
	likely: (26 + 508 + 1280 + 10) / 16 = ~114 turbo cycles
	worst:  (26 + 1016 + 3840 + 10) / 16 = ~305 turbo cycles


---------
lookahead/mixer/push-to-buffer routine runtime per output sample

mixer/lookahead timings
entry + exit:  6 +2+2 +3+4+3 +2 +6 = 28 cycles

each loop:
4+4+4+4+5 +4+4+4+4+5 +2 = 44 cycles per pass

so per-sample, the mixer runtime is:
(44 * 16 + 28) / 16 = ~46 cycles


--------------------
AUDIO DRIVER TIMINGS

For the 4-bit SID driver:

C64: each ISR call runs for 52 cycles.
C128: each runs for 56 cycles including clock stretching, divide by 2 = 28 cycles

SuperCPU:  treat each I/O access as a flat 20 turbo cycles, all other cycle
counts like normal.  There are two I/O accesses in the mono driver and three
in the stereo driver.

Mono mode adds 16 * 2 cycles, for a total of 85 turbo cycles
Stereo mode adds 16 * 3 cycles, for a total of 101 turbo cycles


For the 8-bit DAC driver:

Stock machines:
Same timings as the SID driver if it's a single DAC on the Expansion or User
Port, plus 12 cycles if it's stereo DigiMAX on the User Port, for a maximum of
65 cycles on the C64, or 34, 1 MHz cycles on the C128.

With SuperCPU:
Identical to the SID driver's SuperCPU timings, except add 40 cycles if
it's stereo DigiMAX on the User Port, for a total of 85, 101, or 141 turbo
cycles.


------------
GRAND TOTALS

Using the 4-bit SID driver in mono, with dithering turned off (also applies to
8-bit DAC driver if it's running in mono)...

Factoring-in the mixer/lookahead runtime of 46 cycles per byte, plus 52 cycles
per byte for the ISR, plus the "likely" code path in the fetch routine taking
24 to 26 cycles, plus 20 cycles of fudge to give the sequencer enough time to
run:

The C64 should be run at about 7 kHz with the screen off:

1022727 / (47 + 52 + 26 + 20) = ~7053.3

or about 5.2 KHz with the screen on, since ISR is always 3 rasters:

1022727 / (65 * 3) = ~5244.8

With 24 (1 MHz) cycles for the mixer, 28 for the ISR, 19 for the fetch, and
10 for the sequencer, the C128 should be able to run at about 12.6 kHz:

1022727 / (24 + 28 + 19 + 10) = ~12626.3

Dithering mode, on the other hand is... expensive 😕

SID 4-bit mono with dithering needs 64-66 cycles in the ISR on C64, and 65-69
cycles at 2 MHz (that is, ~33-35 at 1 MHz) on C128.  Stereo needs 86-92 cycles
on C64, 90-96 at 2 MHz (45-48 at 1 MHz) on C128.

Adding in the fudge and other stuff, the C64 just manages to eek-out a 5.2 kHz
(3-raster-line) sample rate with dithering in mono, with the screen on.  With
the screen off, it runs at just shy of 6.2 kHz.  The C128 manages a
respectable 11.7 kHz in this mode.

If all you've got is a stock C64 with only one SID, I highly recommend using
dithering with the screen turned off.  Yes, it's more noise, but it's a much
higher sample rate and a higher effective sample resolution.


===============================
On SuperCPU-equipped machines:

C64 + SuperCPU with screen on will do 15.7 kHz on all modes (the timing could
allow for a much faster rate, but it just rounds up to 1 raster even).

These should need 54 turbo cycles for the mixer, 85 to 141 for the ISR, 114 to
305 for the actual fetch, and 20 cycles' worth of fudge, for a total of 273 to
329 turbo cycles.

20000000 / (46 + 85 + 114 + 20) = ~75471.7
20000000 / (46 + 101 + 114 + 20) = ~71174.4
20000000 / (46 + 141 + 114 + 20) = ~62305.3
20000000 / (46 + 141 + 305 + 20) = ~39062.5

So IN THEORY, such a machine should be able to break 39 kHz as long as VIC-II
screen is turned off, and top out somewhere in the 73 kHz range with the
fastest audio driver and best fetch case....

But in reality? LOL, NOPE!

Not only does none of this account for unexpected slowdowns and cache misses
that I haven't thought of yet, we also have to limit the engine to around 50
kHz or so or certain variables (like the tempo loop counter) will overflow and
others will not be precise enough (like the steprate variables).

Easiest way to limit it here is to use all of the aforementioned worst-case
timings, and round each of the three code sections' timings up to the next
20-turbo-cycle count, and add 20 cycles' worth of fudge:

So (46 + 141 + 305) becomes (60 + 160 + 320 + 20)

Divide by 20 gives (3 + 8 + 16 + 1) in 1 MHz equivalents, values we can use
for the system's timers.

1022727 / (3 + 8 + 16 + 1) = ~36526.0

So about 36.5 kHz, and that's with a really course, conservative limit.  It
may be possible to go even faster, but let's not get greedy... yet. 😛


====
NOTE

None of the above timings take into account convergeance.  As the sample rate
rises, the step rates drop, which decreases the lookahead fetch length, which
in turn further reduces the cycle counts.
