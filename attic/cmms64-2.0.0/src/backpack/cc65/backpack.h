/*

Routine for unpacking backpacked data

*/


/* Unpack data. Returns the number of unpacked bytes. */
unsigned int __fastcall__ back_unpack(unsigned char *dest, unsigned char *src);
