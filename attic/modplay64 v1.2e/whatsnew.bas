start tok64 whatsnew.prg
0 list
0 * what's fixed in modplay64 1.2b *
0 -sid by default, not dac device
0 -mono default, and not stereo
0 -dither disabled, adds only noise
0 -changed "$" with an asm dir routine
0 -reu detected, prints a warning only
0 -onefiled
0 * what's fixed in modplay64 1.2c *
0 -in stereo sid address you can
0 select $d420-$d7e0 and $de00-$dee0
0 -stereo selection de/reactivates
0 address display
0 * what's fixed in modplay64 1.2d *
0 -removed log+pitch calcs by including
0 precalculated and exomized tables
0 -cleaned from scpu specific parts
0 they were unreached code anyway ;)
0 * what's fixed in modplay64 1.2e *
0 -compiled with austrospeed to save
0 some time on mod loading
0:
0 ian coog/hokutoforce  2007.12.10
stop tok64

